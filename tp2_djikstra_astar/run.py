import graphe
import math
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from graphe import voir_chemins

def djikstra(g, depart, arrive):
    E = {}
    dist = {}
    prec = {}
    for i in range(len(g.edges)):
        if g.edges[i] != [] :
            E[i] = g.edges[i]
            dist[i] = math.inf
            prec[i] = None
    dist[depart] = 0
    while E != []:
        mindist = math.inf
        argmin = None
        for u in E:
            if mindist > dist[u]:
                mindist = dist[u]
                argmin = u
        u = argmin
        if u == arrive:
                return (dist, prec)
        temp = E[u]
        del E[u]
        for voisin in temp:
            alt = dist[u] + voisin[1]
            if alt < dist[voisin[0]] :
                dist[voisin[0]] = alt
                prec[voisin[0]] = u

def astar(g, depart, arrive):
    E = {depart}
    dist = {}
    prec = {}
    for i in range(len(g.edges)):
        if g.edges[i] != [] :
            E[i] = g.edges[i]
            dist[i] = math.inf
            f[i] = math.inf
            prec[i] = None
    dist[depart] = 0
    f[depart] = h(depart)
    while E != []:
        mindist = math.inf
        argmin = None
        for u in E:
            if mindist > f[u]:
                mindist = f[u]
                argmin = u
        u = argmin
        if u == arrive:
                return (dist, prec)
        t = E[u]
        del E[u]
        for voisin in t:
            alt = dist[u] + voisin[1]
            if alt < dist[voisin[0]] :
                dist[voisin[0]] = alt
                f[v] = d[v] + h(v)
                prec[voisin[0]] = u
                if v not in E:
                    E = E.append(v)
                
def chemin_opti(prec, dep, arr):
    liste = [arr]
    courrant = arr
    while courrant != dep:
        liste.append(prec[courrant])
        courrant = prec[courrant]
    graphe.voir_chemins(g,[liste])
    return liste

g = graphe.lire_france()
dep = 1
arr = 62
chemin_djikstra = chemin_opti(djikstra(g,dep, arr)[1],dep,arr)
print("chemin djikstra :")
print(chemin_djikstra)
chemin_astar = chemin_opti(djikstra(g,dep, arr)[1],dep,arr)
print("chemin astar :")
print(chemin_astar)
"""voir_chemins(g,chemin_djikstra)"""
