import data_arbres as d
import numpy as np

data = d.load_data('./tp_donnees.csv')

for a in data:
    print(a)
    
""" question 1 """
def proba_empirique(d):
    res = {}
    for data in d:
        y = data.y
        if y in res:
            res[y] += 1 / len(d)
        else:
            res[y] = 1 / len(d)
    return res

""" question 2 """
def question_inf(x, a, s):
    return x[a] < s

""" question 3 """
def split(d, a, s):
    d1 = []
    d2 = []
    for data in d:
        if question_inf(data.x,a,s):
            d1.append(data)
        else:
            d2.append(data) 
    return (d1,d2) 

""" question 4 """
def list_separ_attributs(d, a):
    l = []
    res = []
    for i in d:
        l.append(i.x[a])
    l = list(set(l))
    l.sort()
    for j in range(len(l) - 1):
        res.append((a, (l[j] + l[j + 1]) / 2))
    return res

""" question 5 """
def liste_questions(d):
    l = []
    for a in d[0].x:
        l.append(list_separ_attributs(d, a))
    return l

""" question 6 """
def entropie(d):
    h = 0
    proba = proba_empirique(d)
    for valeur in proba.items():
        if a == 0:
            h = -(h + 0)
        else:
            h = -(h + a) * np.log(valeur)
    return h

""" question 7 """
def gain_entropie(d, q):
    d1 = q[0]
    d2 = q[1]
    r1 = len(d1) / len(d)
    r2 = len(d2) / len(d)
    gain = entropie(d) - r1*entropie(d1) - r2*entropie(d2)
    return gain