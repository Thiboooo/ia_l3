import interfacelib
import numpy as np
import jeulib
import interfacelib
import random

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.couleurval = interfacelib.couleur_to_couleurval(couleur)
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		


class Random(IA):
	
	def demande_coup(self):
    		return random.choice(self.jeu.plateau.liste_coups_valides(self.couleurval))

class Minmax(IA):

	def algo_minmax(self, depth, couleurval, plateau):
			if depth == 0:
				return(None, plateau.evaluation(plateau,couleurval))
			else:
				if couleurval == self.couleurval:
					M = float("-inf")
					for e in plateau.liste_coups_valides(self.couleurval):
						copie_plateau = plateau.copie()
						copie_plateau.jouer(e , couleurval)
						move,val = self.algo_minmax(depth-1, -couleurval, copie_plateau)
						print(val)
						print(move)
						if M < val:
							M = val
							argmax = e
						return (argmax, M)
				else:
					m = float("inf")
					for e in self.jeu.plateau.liste_coups_valides(self.couleurval):
						copie_plateau = plateau.copie()
						copie_plateau.jouer(e , couleurval)
						move,val = self.algo_minmax(depth-1, -couleurval, copie_plateau)
						print(val)
						print(move)
						if m > val:
							m = val
							argmin = e
						return (argmin, m)
	def demande_coup(self):
		minmax = self.algo_minmax(10,self.couleurval,self.jeu.plateau)
		return minmax[0]

class AlphaBeta(IA):

	def algo_alphabeta(self, depth, couleurval, plateau, alpha , beta):
			if depth == 0:
				return(None, plateau.evaluation(plateau,couleurval))
			else:
				if couleurval == self.couleurval:
					M = float("-inf")
					for e in plateau.liste_coups_valides(self.couleurval):
						copie_plateau = plateau.copie()
						copie_plateau.jouer(e , couleurval)
						move,val = self.algo_alphabeta(depth-1, -couleurval, copie_plateau, alpha , beta)
						if M < val:
							M = val
							argmax = e
							alpha = max(alpha,M)
							if alpha >= beta:
								break
						return (argmax, M)
				else:
					m = float("inf")
					for e in self.jeu.plateau.liste_coups_valides(self.couleurval):
						copie_plateau = plateau.copie()
						copie_plateau.jouer(e , couleurval)
						move,val = self.algo_alphabeta(depth-1, -couleurval, copie_plateau, alpha , beta)
						if m > val:
							m = val
							argmin = e
							beta = max(beta,m)
							if alpha >= beta:
								break
						return (argmin, m)
	def demande_coup(self):
		alphabeta = self.algo_alphabeta(10,self.couleurval,self.jeu.plateau, float("-inf") , float("inf"))
		return alphabeta[0]
